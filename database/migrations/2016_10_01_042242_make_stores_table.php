<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->increments('id');
  $table->integer('user_id')->unsigned();

    $table->foreign('user_id')->references('id')->on('users');
    $table->string('name')->index();
    $table->integer('type');
    $table->string('logo');
    $table->string('header');
    $table->string('template');
    $table->string('phone');
    $table->string('city');
    $table->string('country');
    $table->string('bio');
    $table->boolean('auth');
    $table->integer('likes');
    $table->string('alt')->nullable();
    $table->string('lat')->nullable();



                $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
