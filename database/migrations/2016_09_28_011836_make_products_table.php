<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();

    $table->foreign('user_id')->references('id')->on('users');
    $table->string('name');
        $table->text('description');        

    $table->decimal('price', 7, 3);
    $table->integer('discount')->nullable();
    $table->string('coupon')->nullable();
    $table->string('category');
    $table->integer('quantity');
    $table->text('notes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
