
@extends('layouts.app')

@section('content')
<div class="container-fluid" id="search">

  <div class="row" >
    <div  class=" col-xs-12 col-md-8 col-md-offset-2 " style="margin-top:5%;margin-bottom:2.5%">
       <form method="get" action="/search">
 <div class="input-group">
    
      <input name="q" type="text" class="form-control" placeholder="ابحث عن منتج آو منتجر">
        <span class="input-group-btn">
        <button style="border-color:white" class="btn btn-link" type="submit"><span style="color:white" class="glyphicon glyphicon-search"></span></button>
      </span>
  </div><!-- /input-group -->
    </form>

    <br>
    <a style="color:white;text-decoration:none" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
 بحث مخصص <span class="glyphicon glyphicon-menu-down"></span>
</a>

<div class="collapse" id="collapseExample">
  <div style="color:white">
    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
    proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
  </div>
</div>
  </div><!-- /.col-lg-6 -->
 
  </div>

</div>
<div class="container">
  <div class="row">
    <div data-step='2' data-intro='fucccccck me!!' class="col-xs-12 col-md-8" id="sales" style="background-color:#f8f8f8">
<div class="col-xs-12"><h5>الأكثر مبيعا و مشاهدة</h5></div>   
<hr>  <div class="col-xs-6 col-md-3">
        <div class="thumbnail">
      <img src="imgs/music.png" alt="...">
      <div class="caption">
        <h5>Thumbnail label</h5>
        <p>...</p>
      </div>
    </div>
     </div>
 <div class="col-xs-6 col-md-3">
        <div class="thumbnail">
      <img src="imgs/music.png" alt="...">
      <div class="caption">
        <h5>Thumbnail label</h5>
        <p>...</p>
      </div>
    </div>
     </div>
      <div class="col-xs-6 col-md-3">
        <div class="thumbnail">
      <img src="imgs/music.png" alt="...">
      <div class="caption">
        <h5>Thumbnail label</h5>
        <p>...</p>

      </div>
    </div>
     </div>
      <div class="col-xs-6 col-md-3">
        <div class="thumbnail">
      <img src="imgs/music.png" alt="...">
      <div class="caption">
        <h5>Thumbnail label</h5>
        <p>...</p>
      </div>
    </div>
     </div>
      <div class="col-xs-6 col-md-3">
        <div class="thumbnail">
      <img src="imgs/music.png" alt="...">
      <div class="caption">
        <h5>Thumbnail label</h5>
        <p>...</p>
      </div>
    </div>
     </div>
      <div class="col-xs-6 col-md-3">
        <div class="thumbnail">
      <img src="imgs/music.png" alt="...">
      <div class="caption">
        <h5>Thumbnail label</h5>
        <p>...</p>
      </div>
    </div>
     </div>
      <div class="col-xs-6 col-md-3">
        <div class="thumbnail">
      <img src="imgs/music.png" alt="...">
      <div class="caption">
        <h5>Thumbnail label</h5>
        <p>...</p>
      </div>
    </div>
     </div>
      <div class="col-xs-6 col-md-3">
        <div class="thumbnail">
      <img src="imgs/music.png" alt="...">
      <div class="caption">
        <h5>Thumbnail label</h5>
        <p>...</p>
      </div>
    </div>
     </div>
    </div>
    <div class="col-xs-12 col-md-3 col-md-offset-1 sales" style="background-color:#f8f8f8" >
     <div class="col-xs-12"><h5>متاجر مميزة</h5></div>   
     <hr>
<div class="bootcards-list">
  <div class="panel panel-default">
    <div class="list-group">
      <a class="list-group-item" href="#">
        <img src="http://placehold.it/50x50" class="img-rounded pull-right"/>
        <h4 class="list-group-item-heading">Acey, Sofia</h4>
        <p class="list-group-item-text">Masung Corp.</p>
      </a>
      <a class="list-group-item" href="#">
        <img src="http://placehold.it/50x50" class="img-rounded pull-right"/>
        <h4 class="list-group-item-heading">Barish, Joseph</h4>
        <p class="list-group-item-text">Masung Corp.</p>
      </a>
      <a class="list-group-item" href="#">
        <img src="http://placehold.it/50x50" class="img-rounded pull-right"/>
        <h4 class="list-group-item-heading">Benson, Sarah</h4>
        <p class="list-group-item-text">ZetaComm</p>
      </a>
       <a class="list-group-item" href="#">
        <img src="http://placehold.it/50x50" class="img-rounded pull-right"/>
        <h4 class="list-group-item-heading">Benson, Sarah</h4>
        <p class="list-group-item-text">ZetaComm</p>
      </a>
       <a class="list-group-item" href="#">
        <img src="http://placehold.it/50x50" class="img-rounded pull-right"/>
        <h4 class="list-group-item-heading">Benson, Sarah</h4>
        <p class="list-group-item-text">ZetaComm</p>
      </a>
       <a class="list-group-item" href="#">
        <img src="http://placehold.it/50x50" class="img-rounded pull-right"/>
        <h4 class="list-group-item-heading">Benson, Sarah</h4>
        <p class="list-group-item-text">ZetaComm</p>
      </a>
       <a class="list-group-item" href="#">
        <img src="http://placehold.it/50x50" class="img-rounded pull-right"/>
        <h4 class="list-group-item-heading">Benson, Sarah</h4>
        <p class="list-group-item-text">ZetaComm</p>
      </a>
    </div>
  </div>
</div>
     
    </div>
<div class="col-xs-12 promo"><div class="col-md-8">    <h3>هل لديك منتج جديد أو مستخدم تريد بيعه؟ </h3>
        <h3>هل تمتلك متجر أو تقدم خدمة من أي نوع؟</h3>
        <h2>ابدأ معنا و اعرض منتجك للبيع و انشئ متجرك الإلكتروني في دقائق!</h2>
<a style="color:#3399ff" href="register" class="btn btn-default btn-lg btn-block">تسجيل <span class="glyphicon glyphicon-menu-left"></span></a>
<br>
</div>
<div class="col-md-4 promo1"></div>
    
    </div>
  </div>
</div>

   
@endsection
